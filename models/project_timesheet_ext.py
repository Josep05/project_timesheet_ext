# -*- coding: utf-8 -*-
################################################################
#    License, author and contributors information in:          #
#    __openerp__.py file at the root folder of this module.    #
################################################################

from openerp import models, fields, api, _
from openerp.exceptions import Warning
import openerp.addons.decimal_precision as dp
import logging

_logger = logging.getLogger(__name__)

###############################################################################
#   model.inherit                                                             #
###############################################################################

class ProjectTask(models.Model):

    _inherit = 'project.task'

    # --------------------------- ENTITY  FIELDS ------------------------------

    name_module = fields.Char(
        comodel_name='name_module',
        string='Name of module',
    )
    
    function_module = fields.Char(
        comodel_name='function_module',
        string='Funcionality',
    )
    
    url_github = fields.Char(
        comodel_name='url_github',
        string='Link to github / gitlab',
    )
    
    description = fields.Html(
        comodel_name='description',
        string='Description',
    )
     
    
    

    # ----------------------- AUXILIARY FIELD METHODS -------------------------

